jQuery(document).ready(function(){
   jQuery.ajaxSetup({ cache: false });
   template();
   jQuery.ajax({
      url: xmlFile,
      success: function(xml){
         jQuery(xml).find("items formitem").each(function(){
            
            jQuery(this).find("item").each(function(){
               var cls    = jQuery(this).find("classname").text();
               var title  = jQuery(this).find("title").text();
			   var ids  = jQuery(this).find("ids").text();
               var wsize  = jQuery(this).find("wsize").text();
               var hissu  = jQuery(this).find("hissu").text();
               var ftype  = jQuery(this).find("formtype").text();
               var infom  = jQuery(this).find("infom").text();
               var val    = null;
               
               switch(ftype){
                  case 'mil':
                     val = '<input type="text" name="'+cls+'" id="'+cls+'" size="35" value="" class=" '+valid(hissu,ftype)+'">';
                     val += '<br /><span class="fmemo">'+infom+'</span>';
                  break;
                  case 'stx':
                     val = '<input type="text" name="'+cls+'" id="'+cls+'" size="'+wsize+'" value="" class=" '+valid(hissu,ftype)+'">';
                     val += '<br /><span class="fmemo">'+infom+'</span>';
                  break;
                  case 'wtx':
                     val = '<textarea name="'+cls+'" id="'+cls+'" class=" '+valid(hissu,ftype)+'"></textarea>';
                     val += '<br /><span class="fmemo">'+infom+'</span>';
                  break;
                  case 'ara':
                     case 'sel':
                     val = '<select name="'+cls+'" id="'+cls+'" class=" '+valid(hissu,ftype)+'">';
                     val += '<option value="">▽</option>';
                     
                     jQuery(this).find("sel item").each(function(){
                        var name  = jQuery(this).text();
                        val += '<option value="'+name+'">'+name+'</option>';
                     });
                     val += '</select>';
                     val += '<br /><span class="fmemo">'+infom+'</span>';
                  break;
                  case 'rdo':
                     val = '';
                     var p=0;
                     jQuery(this).find("sel item").each(function(){
                        var name  = jQuery(this).text();
                        val += '<label><input type="radio" name="'+cls+'" id="rdo'+cls+'" value="'+name+'" class=" '+valid(hissu,ftype)+'"> '+name+' </label>';
                        p++;
                     });
                     val += '<br /><span class="fmemo">'+infom+'</span>';
                  break;
                  case 'ckb':
                     val = '';
                     var p=0;
                     jQuery(this).find("sel item").each(function(){
                        var name  = jQuery(this).text();
						if( p != 0){
							val += '<br>';
						}
                        val += '<label><input type="checkbox" name="'+cls+'[]" id="'+cls+'" value="'+name+'"  class=" '+valid(hissu,ftype)+'"> '+name+' </label>';
                        p++;
                     });
                     val += '<br /><span class="fmemo">'+infom+'</span>';
                  break;
               }
               step1(cls,title,ids,val,hissu);
            });
         });
      }
   });
   var step1 = function(cls,title,ids,val,hissu){
      if( title ) {
		 title = title+'　<img src="../images/form-'+ids+'.png">';
         var title1 = title;
         if( hissu == 1) { title1 = title+' <span class="hissu">※</span>'; }
         // var step1 = '<p class="f_title">'+title1+'</p><p class="f_val">'+val+'</p>';
         // var step2 = '<p class="f_title">'+title+'</p><p class="f_val"><span class="'+cls+'"></span></p>';
         var step1 = '<tr><th scope="row" class="f_title" id="'+ids+'">'+title1+'</th><td class="f_val">'+val+'</td></tr>';
         var step2 = '<tr><th scope="row" class="f_title">'+title+'</th><td class="f_val"><span class="'+cls+'"></span></td></tr>';	  
         jQuery("#step1 .ftable").append(step1);
         jQuery("#step2 .ftable").append(step2);
      }
   }
   var valid = function(hissu,type){
      var valid = '';
      switch(type){
         case 'mil':
            valid = '  validate[required,custom[email]] text-input';
         break;
         case 'stx':
            valid = ' validate[required] text-input';
         break;
         case 'ara':
         case 'sel':
         case 'wtx':
            valid = 'validate[required]';
         break;
         case 'rdo':
            valid = 'validate[required] radio';
         break;
         case 'ckb':
            valid = ' validate[required] checkbox';
         break;
      }
      
      if( hissu ) return valid;
   }

   jQuery("#FormStep1").validationEngine( 'attach', {
        ajaxFormValidation: true
      , onBeforeAjaxFormValidation: beforeCall
      ,promptPosition:"bottomLeft"
   });
   
   function beforeCall(){
      jQuery("#step1").hide();
      jQuery("#step2").show();
      var str = jQuery("#FormStep1").serialize();
      jQuery.ajax({
         url: xmlFile,
         success: function(xml){
            jQuery(xml).find("items formitem").each(function(){
               jQuery(this).find("item").each(function(){
                  var cls    = jQuery(this).find("classname").text();
                  var ftype  = jQuery(this).find("formtype").text();
                  
                  if(cls){
                     var divid = '.'+cls;
                     var val = jQuery("#"+cls).val();
                     if(ftype == 'wtx') { val = val.replace(/\n/g, "<br />"); }
                     if(ftype == 'rdo') { val = jQuery("#rdo"+cls+":checked").val(); }
                     if(ftype == 'ckb') {  var val = ''; jQuery('[id="'+cls+'"]:checked').each(function(){ val += jQuery(this).val() +'<br />'; });
                     }
                     jQuery(divid).html(val);
                  }
               });
            });
         }
      });
   }
   jQuery(document).on('click','#backButton',function(e){
      jQuery("#step1").show();
      jQuery("#step2").hide();
   });
   jQuery(document).on('click','#sendBUtton',function(e){
      var data = jQuery("#FormStep1").serialize();
            jQuery("#step2").hide();
            jQuery("#step3").html('<img src="'+filePath+'lorder.gif" />');
      jQuery.ajax({
         async:false,
         type: "POST",
         url: filePath+"sendmail.php",
         data: data,
         success: function(xml){
            jQuery("#step3").html(LastMessage);
         }
      });
   });
});

function template(){
   jQuery("#contact_container #koko").html('<img src="'+filePath+'lorder.gif" />');
   var tpl  = '';
       tpl  = '<div id="step1"><form id="FormStep1" name="FormStep1" method="post"><table><thead><tr><th scope="col" colspan="2">Contact form</th></tr></thead><tbody class="ftable"></tbody></table><div class="button"><input type="image" src="http://www.tabunkanpo.org/images/btn-form.png" alt="送信する" align="middle"></div></form>';
       tpl += '</div><div id="step2"><table><thead><tr><th scope="col" colspan="2">Contact form</th></tr></thead><tbody class="ftable"></tbody></table><div class="formbutton"> <input type="image" src="http://www.tabunkanpo.org/images/btn-back.png" id="backButton" value="&Lt; 訂正"><input type="image" src="http://www.tabunkanpo.org/images/btn-send.png" id="sendBUtton" value="メールを送信する &Gt;"></div></div><div id="step3"></div>';   
   jQuery("#contact_container #koko").html(tpl);
}
