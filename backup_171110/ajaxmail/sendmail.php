<?php
error_reporting(0);
mb_language("Japanese");
mb_internal_encoding("UTF-8");

// 管理者側メール冒頭文内容
$admin_body  = "下記の内容でお問い合わせが御座いました\n";
$admin_body .= "\n";

//管理者側メールサブジェクト
$admin_subject = "[MAIL]お問い合わせフォームより";

//管理者メールアドレス
$frm = "info@tabunkanpo.org";
  
//*************************************************************************************
/*
 * 以下は変更不可
 */

$file  = file_get_contents("8b29b609f0a70b9256c54f0b8f7ad089.xml");
$xml   = array();
$xml   = simplexml_load_string($file);
$obj   = xml_row($xml);

if( is_array($obj["text1"]) )$obj["text1"] = "";
if( is_array($obj["text2"]) )$obj["text2"] = "";

$sbj   = $obj["subject"];
$ftr   = $obj["text2"];
$head        = $obj["text1"]."\n";
$array       = array();
$email_array = array();
$body        = "";


  $hidden      = "\n==============================================\n";
  $hidden     .= "□隠し項目\n";
  foreach ($obj["formitem"]["item"] AS $item){
     if( $item["formtype"] == "ckb" ){
        $body .= "-----------------------------\n";
        $body .= "□".$item["title"]."\n";
        $arr = $_POST[$item["classname"]];
        for( $i=0; $i<count($arr); $i++ ) {
           $body .= $arr[$i] ."\n";
        }
     } elseif( $item["formtype"] == "hid" ){
        $hidden .= $item["title"].":".$item["infom"] ."\n";
     } else{
        $body .= "-----------------------------\n";
        $body .= "□".$item["title"]."\n";
        $body .= $_POST[$item["classname"]]."\n";
     }
     if( $item["formtype"] == "mil" ){
        $email_array[] = $_POST[$item["classname"]];
     }
  }


$foot = "\n".$obj["text2"];

foreach( $email_array AS $sendmail) {
   sendMail($head.$body.$foot,$sbj,$sendmail,$frm); //ユーザーへ
}
   sendMail($admin_body.$body.$hidden,$admin_subject,$frm,$sendmail); //管理者へ
   
function xml_row($xmlobj) {
   $arr = array();
   if (is_object($xmlobj)) {
      $xmlobj = get_object_vars($xmlobj);
   } else {
      $xmlobj = $xmlobj;
   }
   foreach ($xmlobj as $key => $val) {
      if (is_object($xmlobj[$key])) {
         $arr[$key] = xml_row($val);
      } else if (is_array($val)) {
         foreach($val as $k => $v) {
            if (is_object($v) || is_array($v)) {
               $arr[$key][$k] = xml_row($v);
            } else {
               $arr[$key][$k] = $v;
            }
         }
      } else {
         $arr[$key] = $val;
      }
   }
   return $arr;
}

/*
 * メール送信処理
 * PHPの基本的な設定で書いていますので、変更する場合はこちらを調整
 */
 function sendMail($body,$subject,$sendmail,$from){
   $header  = "From: $from"."\n"."X-Priority: 1"."\n"."X-Mailer: PHP/".phpversion();  
   mb_send_mail($sendmail,$subject,$body,$header);
 }
?>
   