
$(document).ready(function() {
		//スライダー
        $('a[href^=#]').click(function() {
            var speed = 500;
            var href = $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top;
            $("html, body").animate({
                scrollTop: position
            }, speed, "swing");
            return false;
        });
		
		$('.bxslider').bxSlider({
                auto: true
        })
		
        //ヘッダー
        var nav = $('.normal-header');
        //表示位置
        var navTop = nav.offset().top + 550;
        //ナビゲーションの高さ（シャドウの分だけ足してます）
        var navHeight = nav.height() + 10;
        var showFlag = false;
        nav.css('top', -navHeight + 'px');
        //ナビゲーションの位置まできたら表示
        $(window).scroll(function() {
            var winTop = $(this).scrollTop();
            if (winTop >= navTop) {
                if (showFlag == false) {
                    showFlag = true;
                    nav
                        .addClass('fixed')
                        .stop().animate({
                            'top': '0px'
                        }, 1000);
                }
            } else if (winTop <= navTop) {
                if (showFlag) {
                    showFlag = false;
                    nav.stop().animate({
                        'top': -navHeight + 'px'
                    }, 1000, function() {
                        nav.removeClass('fixed');
                    });
                }
            }
        });
		
		//ページトップボタン	
        var topBtn = $('#pagetop');
        topBtn.hide();
        //スクロールが100に達したらボタン表示
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                topBtn.fadeIn();
            } else {
                topBtn.fadeOut();
            }
        });
        //スクロールしてトップ
        topBtn.click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 500);
            return false;
        });
	
		
		
});

//POP Window
function commonPop(pFile, pWidth, pHeight,pStatus){
	var wName = "";
	if(pWidth == null){ pWidth = 700}
	if(pHeight == null){ pHeight = 600}
	if(pStatus == null){ pStatus = 'yes';}
	if(pWidth > screen.availWidth){
		pWidth = screen.availWidth;
		pStatus = 'yes';
	}
	if(pHeight > screen.availHeight-50){
		pHeight = screen.availHeight-50;
		pStatus = 'yes';
	}
	var wFeatures= 'menubar=no,scrollbars='+pStatus+',resizable='+pStatus+',width='+pWidth+',height='+pHeight;
	void(window.open(pFile, wName, wFeatures));
}