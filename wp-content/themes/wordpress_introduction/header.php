<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<title><?php echo trim(wp_title('', false)); if(wp_title('', false)) { echo ' - '; } bloginfo('name'); ?></title>
<meta name="description" content="<?php echo trim(wp_title('', false)); if(wp_title('', false)) { echo ' - '; } bloginfo('description'); ?>" />
<meta name="keywords" content="多文化,多文化共生,多文化共生支援センター,留学生,留学生支援">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />



<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php
if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
?>

<link href="<?php echo esc_url( home_url() ); ?>/css/normalize.css" rel="stylesheet">
<link href="<?php echo esc_url( home_url() ); ?>/css/styles.css" rel="stylesheet">
<link href="<?php echo esc_url( home_url() ); ?>/css/jquery.bxslider.css" rel="stylesheet">
<link href="<?php echo esc_url( home_url() ); ?>/css/lightbox.css" rel="stylesheet">

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

<script src="<?php echo esc_url( home_url() ); ?>/js/jquery.min.js"></script>
<script src="<?php echo esc_url( home_url() ); ?>/js/jquery.bxslider.min.js"></script>
<script src="<?php echo esc_url( home_url() ); ?>/js/lightbox.min.js"></script>
<script src="<?php echo esc_url( home_url() ); ?>/js/common.js"></script>

<?php if ( is_page('14') ): ?>
<link href="<?php echo esc_url( home_url() ); ?>/ajaxmail2/validationEngine.jquery.css" rel="stylesheet">
<link href="<?php echo esc_url( home_url() ); ?>/ajaxmail2/style.css" rel="stylesheet">
<script src="<?php echo esc_url( home_url() ); ?>/ajaxmail2/jquery.validationEngine.js"></script>
<script src="<?php echo esc_url( home_url() ); ?>/ajaxmail2/jquery.validationEngine-ja.js"></script>
<script src="<?php echo esc_url( home_url() ); ?>/ajaxmail2/sendmail.js"></script>
<?php else: ?>
<link href="<?php echo esc_url( home_url() ); ?>/ajaxmail/nomal.css" rel="stylesheet">
<script src="<?php echo esc_url( home_url() ); ?>/ajaxmail/include.js"></script>
<?php endif; ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-57165077-1', 'auto');
  ga('send', 'pageview');
</script>

<?php wp_head(); ?>

</head>


<body id="top">

<header class="wrap clearfix">
<div class="normal-header">

<div id="language-nav">
<?php if ( is_home() || is_front_page() ): ?>
日本語 / <a href="<?php echo esc_url( home_url() ); ?>/en/">English</a>
<?php elseif ( is_page('14') ): ?>
<a href="<?php echo esc_url( home_url() ); ?>/">日本語</a> / English
<?php endif; ?>
</div>

<div id="header-logo">
<img src="<?php echo esc_url( home_url() ); ?>/images/logo.jpg" width="174" height="92" alt="特定非営利活動法人　多文化共生支援センター"/>
</div>
<nav>
<ul id="gnav">
<li><a href="#active"><img src="<?php echo esc_url( home_url() ); ?>/images/gnav-active.png" width="91" height="28" alt="活動報告"/></a></li>
<li><a href="#about"><img src="<?php echo esc_url( home_url() ); ?>/images/gnav-about.png" width="80" height="28" alt="私たちについて"/></a></li>
<li><a href="#outline"><img src="<?php echo esc_url( home_url() ); ?>/images/gnav-outline.png" width="67" height="28" alt="団体概要"/></a></li>
<li><a href="#join"><img src="<?php echo esc_url( home_url() ); ?>/images/gnav-join.png" width="108" height="28" alt="入会案内"/></a></li>
<li><a href="#contact"><img src="<?php echo esc_url( home_url() ); ?>/images/gnav-contact.png" width="74" height="28" alt="お問い合せ"/></a></li>
</ul>
</nav>
</div>

</header>