<?php get_header(); ?>

<div id="slider" class="wrap">
	<ul class="bxslider">
		<li><img src="images/main-img-01.jpg" alt="" /></li>
		<li><img src="images/main-img-02.jpg" alt="" /></li>
		<li><img src="images/main-img-03.jpg" alt="" /></li>
	</ul>
</div>
<div id="content" class="wrap">
<section>

<br><br>

<h1><img src="images/news-title.png" width="136" height="94" alt="新着情報　NEWS"/></h1>


<div id="info">



<dl>
<dt><strong>2018.01.11（木）</strong></dt>
<dd>2018.02.17（土）9：30～
<br><strong><font size="4" color="blue">『採用面接なんてこわくない！』</font></strong>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/newlogo.gif" width="58.8" height="25.2" alt="new" />
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<strong><u><font size="3">(企業情報と面接対策)</font></u></strong><br>
外国人留学生のための<font size="3"><strong>第2回就職セミナー</strong></font>を開催します。<br>
<font size="3"><strong>第一部　面接セミナー</strong></font><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;(採用面接に隠された企業側の意図とは)<br>
<font size="3"><strong>第二部　企業セミナー</strong></font><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;(・当社が欲しい留学生とは？　・当社の留学生の受け入れ状況と海外戦略)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;◇「IT業界と創研情報（株）」<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;◇「経営戦略と採用方針」<br>
参加費：無料<br>
定員：５０名(先着順とさせていただきます)<br>
※今回参加の方で、企業への就職希望者には個別面接予約を行っていただきます。<br>
就職希望、就職活動をされているあるいは就職に関心のある外国人留学生の方々の参加を<br>
お待ちしています。<br>
【お問合せ・申し込み先】<a href="mailto:info@tabunkanpo.org">✉Mail：info@tabunkanpo.org<a/></dt><a href="pdf/application.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="images/pdficon_large.png" width="20" height="20" alt="pdf" />申し込み書</a>
 <hr></dl>

<dl>
<dt><strong>2018.01.11（木）</strong></dt>
<dd>2017.02.25（日）～02.26（月）
<br><strong><font size="3" color="blue">1泊2日！六日町スキーリゾート</font></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/newlogo.gif" width="58.8" height="25.2" alt="new" />
<br>新潟県でスキーツアーを開催します！<br>
東京から８０分、川端康成の著書「雪国」の町<br>
「日本アルプススキーと温泉和食と日本酒の町<br>
<img src="images/detaillogo.gif" width="110.1" height="18" alt="detail" /><a href="pdf/ski tour18.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="images/pdficon_large.png" width="20" height="20" alt="pdf" />&nbsp;新幹線でLet`s Ski~!!Enjyo!!留学生活！！</a>
<br>【お問合せ・申し込み先】<a href="mailto:info@tabunkanpo.org">✉Mail：info@tabunkanpo.org</a></dt>
 <hr></dl>


<dl>
<dt><strong>2018.01.11（木）</strong></dt>
<dd>2017.2.2（金）　18：00～　
<br><font size="3"><strong>第2回「多文化ゼミナール」</strong>を開催します！</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/newlogo.gif" width="58.8" height="25.2" alt="new" />
<br>“みなさんは、「パブリック・ディプロマシー」という言葉を聞いたことがありますか。<br>
「文化」とは何か、「日本」とは何か、「日本を紹介する」とは、どういうことか、<br>留学生のみなさんと一緒に考えてみたいのです。<br>
<strong>【参加費】無料</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;※<u>ディスカッションは英語でも可です。</u>
<br><img src="images/detaillogo.gif" width="110.1" height="18" alt="detail" /><a href="pdf/tabunkazemi2.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="images/pdficon_large.png" width="20" height="20" alt="pdf" />&nbsp;第2回「多文化ゼミナール」</a>
<br>【お問合せ・申し込み先】<a href="mailto:info@tabunkanpo.org">✉Mail：info@tabunkanpo.org</a></dt>
 <hr></dl>


<dl>
<dt><strong>2017.11.14（火）</strong></dt>
<dd>2017.12.09（土）　15：00～<br><strong>第4回インターナショナルクリスマスパーティ</strong>を開催します！<br>
世界中の国々から日本に訪れる方々のために、おもてなしをみんなで経験しませんか。興味のある方は是非ご参加ください！日本の企業で就職を担当されている方々も参加します♪。</p></p>
【場所】DUBLINERS' IRISH PUB赤坂：千代田区永田町2-11-1山王パークタワー B1F
【会費】学生　お一人　2,000円/一般　お一人　3,000円<a href="pdf/xmas2017.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語版</a><br><font color="red">※終了いたしました。</font></dt>
 <hr></dl>

<dl>
<dt><strong>2017.10.27（金）</strong></dt>
<dd>2017.11.07（火）　18：00～　<strong>第1回多文化ゼミナール</strong>を開催します！<br>
「文化」といっても人によっていろいろなことを考えると思います。<br>
その広がりから「文化とは何か？」を考えてみましょう。<br>
例えば「食文化」。日本の食文化と留学生の母国の食文化、どう違っているのか？<br>
…など、身近な「文化」の話も含めてみんなで話し合いましょう。</p></p>
【場所】特定非営利活動法人（NPO）多文化共生支援センター　2F<br>
【会費】無料<a href="pdf/tabunkasemi1.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語版</a><font color="red">※終了いたしました。</font></dt>
 <hr></dl>


<dl>
<dt><strong>2017.01.12（木）</strong></dt>
<dd>2017.02.04（土）13：30～　外国人留学生のための就職セミナーを開催します。
<br>『採用面接を成功させよう！』<br>
外国人留学生のための就職セミナー(企業情報と面接対策)<br>
第一部　企業セミナー(・当社がほしい人財は？　・当社の現状と展開戦略)<br>
第二部　面接セミナー(採用面接に隠された企業側の意図とは)
参加費：無料<br>
定員：５０名(先着順とさせていただきます)<br>
就職希望、就職活動をされているあるいは就職に関心のある外国人留学生の方々の参加を<br>
お待ちしています。<a href="pdf/shusyokuannai2017.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="images/pdficon_large.png" width="20" height="20" alt="pdf" />就職セミナー案内</a>　<a href="pdf/shukatsu2017.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="images/pdficon_large.png" width="20" height="20" alt="pdf" />申込書PDF</a>　<a href="pdf/shukatsu2017.docx" style="text-decoration:none" target="_blank" alt="pdf" /><img src="images/wordicon_large.png" width="20" height="20" alt="pdf" />申込書word</a><font color="red">※終了いたしました。</font></dt>
</dl>

 
 <h2><img src="images/past-event-title.png" width="172" height="22" alt="PAST EVENTS"/></h2>
<hr>
<dl>
<dt><strong>2016.12.10（土）</strong></dt>
<dd><p><a href="pdf/xmas2016.pdf" style="text-decoration:none" target="_blank" alt="pdf" />第3回インターナショナルクリスマスパーティを開催しました！</a></p></dd>
</dl>

<dl>
<dt><strong>2016.02.13（土）</strong></dt>
<dd><p><a href="pdf/egypt_cooking_japan.pdf" style="text-decoration:none" target="_blank" alt="pdf" />エジプトの食生活を学ぼう！を開催しました！</a></p></dd>
</dl>

<dl>
<dt><strong>2016.02.05（金）～02.06（土）</strong></dt>
<dd><p><a href="pdf/Ski tour.pdf" style="text-decoration:none" target="_blank" alt="pdf" />1泊2日！六日町スキーリゾート(新潟県)で<br>スキー/スノボツアーを開催しました！</a></p></dd>
</dl>

<dl>
<dt><strong>2015.12.12（土）</strong></dt>
<dd> 
<p><a href="pdf/Sushi party.pdf" style="text-decoration:none" target="_blank" alt="pdf" />巻きずしパーティー開催しました！</a></p></dd>
</dl>

<dl>
<dt><strong>2015.10.21（水）</strong></dt>
<dd><a href="pdf/rental space.pdf" style="text-decoration:none" target="_blank" alt="pdf" />2015-2016レンタルスペースのご案内</a></p></dd>
</dl>

<dl>
<dt><strong>2015.07.18（土）</strong></dt>
<dd><p><a href="pdf/kawagoe Seven Lucky Gods Tour.pdf" style="text-decoration:none" target="_blank" alt="pdf" />小江戸川越七福神めぐりを開催しました！</a></p>
</dd>
</dl>

<dl>
<dt><strong>2015.06.27（土）</strong></dt>
<dd><p><a href="pdf/zoshigaya Seven Lucky Gods Tour.pdf" style="text-decoration:none" target="_blank" alt="pdf" />雑司ヶ谷七福神めぐりを開催しました！</a></p>
</dd>
</dl>
 <hr>
<dl>
<dt><strong>2015.03.12（木）</strong></dt>
<dd><p>facebookはじめました！</p>
<p>いいね！たくさんくださいね！　<a href="https://www.facebook.com/pages/%E5%A4%9A%E6%96%87%E5%8C%96%E5%85%B1%E7%94%9F%E6%94%AF%E6%8F%B4%E3%82%BB%E3%83%B3%E3%82%BF%E3%83%BC/349404461913132?ref=hl" target="_blank">NPO法人 多文化共生支援センター　公式Facebook</a></p></dd>
</dl>

</div>






<br><br>

</section>




<section>
    	<div id="active">
        	<h1><img src="images/active-title.png" width="302" height="94" alt="活動報告　ACTIVITIES"/></h1>
            <div class="photo">
                  <div class="col">
                  <img src="images/active-pict-20.jpg" width="445" height="330" alt="2017年12月9日 第4回クリスマスパーティー"/>
                    <p>2017年12月9日<br>
                    <a href="pdf/xmax2017-re.pdf" style="text-decoration:none" target="_blank" alt="pdf" />第4回クリスマスパーティー</a></p>
                </div><div class="col">
                  <img src="images/active-pict-19.JPG" width="445" height="330" alt="2017年11月17日～19日 ニューヨークアニメフェア"/>
                    <p>2017年11月17日～19日<br>
                    ニューヨークアニメフェア</p>
                </div><div class="col">
                  <img src="images/active-pict-18.jpg" width="445" height="330" alt="2017年11月8日 南アフリカ大使館"/>
                    <p>2017年11月8日<br>
                    南アフリカ大使館</p>
                </div><div class="col">
                  <img src="images/active-pict-17.jpg" width="445" height="330" alt="2017年11月7日 多文化セミナー"/>
                    <p>2017年11月7日<br>
                    多文化セミナー</p>
                </div><div class="col">
                  <img src="images/active-pict-16.JPG" width="445" height="330" alt="2017年2月4日 外国人留学生のための就職セミナー"/>
                    <p>2017年2月4日<br>
                    外国人留学生のための就職セミナー</p>
                </div><div class="col">
                  <img src="images/active-pict-14.jpg" width="445" height="330" alt="2016年2月13日 エジプト食生活イベント"/>
                    <p>2016年2月13日<br>
                    エジプト食生活イベント</p>
                </div><div class="col">
                  <img src="images/active-pict-13.jpg" width="445" height="330" alt="2016年1月29日～31日 スキー・スノーボード教室"/>
                    <p>2016年1月29日～31日<br>
                    スキー・スノーボード教室</p>
                </div><div class="col">
                  <img src="images/active-pict-12.jpg" width="445" height="330" alt="2015年12月12日 巻き寿司パーティ"/>
                    <p>2015年12月12日<br>
                    巻き寿司パーティ</p>
                </div><div class="col">
                  <img src="images/active-pict-11.jpg" width="445" height="330" alt="2015年9月15日 2015年大相撲観戦"/>
                    <p>2015年9月15日<br>
                    大相撲観戦</p>
                </div><div class="col">
                    <img src="images/active-pict-21.jpg" width="445" height="330" alt="2015年6月24日 JAL工場見学"/>
                  <p>2015年6月24日<br>
                   JAL工場見学</p>
                </div><div class="col">
                    <img src="images/active-pict-08.jpg" width="445" height="330" alt="2015年2月21日 スキー教室"/>
                  <p>2015年2月21日<br>
                  スキー教室</p>
                </div><div class="col">
                    <img src="images/active-pict-05.jpg" width="445" height="330" alt="2014年8月26日 三井ホーム株式会社　モデルハウス見学会"/>
                    <p>2014年8月26日<br>三井ホーム株式会社　モデルハウス見学会</p>
                </div>
          </div>
        </div><!-- /active -->
        
            <div id="past-event">
                <div class="past-event-wrap clearfix">
                    <h2><img src="images/past-event-title.png" width="172" height="22" alt="PAST EVENTS"/></h2>
                    <ul class="past-photo">
                    <li><h3>大相撲観戦</h3>
                            <ul class="col">
                                <li><a href="images/past-event-pict-01.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-01.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-02.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-02.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-03.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-03.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-04.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-04.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-05.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-05.jpg" width="110" height="80" alt=""/></a></li>
                            </ul>
                        </li>
                    <li>
                            <h3>スイスお茶会セミナー</h3>
                            <ul class="col">
                                <li><a href="images/past-event-pict-11.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-11.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-12.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-12.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-13.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-13.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-14.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-14.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-15.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-15.jpg" width="110" height="80" alt=""/></a></li>
                            </ul>
                        </li>
                        <li><h3>クリスマスパーティー</h3>
                            <ul class="col">
                                <li><a href="images/past-event-pict-06.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-06.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-07.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-07.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-08.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-08.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-09.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-09.jpg" width="110" height="80" alt=""/></a></li><li><a href="images/past-event-pict-10.jpg" data-lightbox="pastevent"><img src="images/past-event-pict-thumb-10.jpg" width="110" height="80" alt=""/></a></li>
                            </ul>
                        </li>
                        
                                
                            </ul>
                        </li>
                    </ul>
                    </div><!-- /past-event-wrap -->
                    <p id="events-archive"><a href="javascript:commonPop('events_archive.html',950,700);">過去の活動報告はこちら<span>&raquo;</span></a></p>
                </div><!-- /past-event -->
             </section>   
        <section>
			<div id="about">
                <div class="cont-wrap">
                    <h1><img src="images/about-title.png" width="236" height="93" alt="私たちについて　ABOUT5 US"/></h1>
                    <h2><img src="images/about-title-02.png" width="128" height="26" alt="理事長挨拶"/></h2>
                    <p>私は、30年にわたって、外国人に対する日本語学校（外務省、法務省、文部科学省認可校）の経営をしてまいりました。<br>
日本語の習得はもちろん、日本での生活支援、相互文化交流などを通じて多くの留学生たちを支援し、２万人を超える卒業生を、大学・大学院や企業へと送り出しました。それと同時に、常に20～30ヶ国とやり取りをした経験から、相互理解の難しさを肌で感じ、いかにコミュニケーションをとることが大事か、ということも痛感してきました。「多文化共生支援センター」では、日本人と外国人が互いの文化・習慣の違いを、様々な交流を通して理解し合えるためにサポート活動を行いたいと思います。</p>
                    <p class="txt-serif txt-middle txt-right txt-bold margin-top10">中澤　百百子</p>
                    <p class="pdf-link"><img src="images/material-title.png" width="108" height="17" alt="MATERIAL"/><a href="pdf/The greeting of president.pdf" target="_blank">The greeting of president</a></p>
                </div>
              <div id="purpose" class="clearfix">
                    <div class="cont-wrap">
                        <div id="purpose-title">
                            <h3><img src="images/about-col-title.png" width="270" height="80" alt="NPO法人　多文化共生支援センターの目的"/></h3>
                            <p>地球上の人々が、様々な違いのある文化や習慣に互いに興味を持ち、理解を深め、人生を楽しく高められるように、人の温もりと輪を大切に、海外からの留学生のサポート活動を行う。</p>
                        </div>
                        <ol>
                            <li><img src="images/about-col-01.png" width="333" height="244" alt=""/></li>
                            <li><img src="images/about-col-02.png" width="333" height="244" alt=""/></li>
                            <li><img src="images/about-col-03.png" width="333" height="244" alt=""/></li>
                            <li><img src="images/about-col-04.png" width="333" height="244" alt=""/></li>
                            <li><img src="images/about-col-05.png" width="333" height="244" alt=""/></li>
                        </ol>
                    </div><!-- /cont-wrap -->
                </div><!-- /purpose -->
                <div id="vision">
                    <h3><img src="images/vision-title.png" width="174" height="53" alt="ビジョン"/></h3>
                    <p>日本人と外国人が<br>
                    互いの文化・習慣の違いを、<br>
                    様々な交流を通して理解し合えるために<br>
                    サポート活動を行う。</p>
                </div><!-- /vision -->
                <div id="mission">
                    <h3><img src="images/mission-title.png" width="220" height="55" alt="短期ミッション"/></h3>
                    <p>東京オリンピック・パラリンピック2020<br>
                    参加国の自国見学者の通訳、<br>
                    東京の道先案内人を担う<br>
                    留学生人材の育成に、提供に寄与する。</p>
                </div><!-- /mission -->
              <div id="thema">
                    <h3><img src="images/5therma-title.png" width="229" height="58" alt="5つの活動テーマ"/></h3>
                    <p class="pdf-link"><img src="images/material-title-white.png" width="108" height="17" alt="MATERIAL"/><br>
                <a href="pdf/Action policy - 5 action programmes.pdf" target="_blank">Action policy-5 action programs</a></p>
			</div><!-- /mission -->
	  </div><!-- /about -->
      </section>
      <section>
        <div id="outline" class="clearfix">
        	<h1><img src="images/outline-title.png" width="208" height="93" alt="団体概要　OUTLINE"/></h1>
            <div class="clearfix">
                <div class="left-cont">
                    <table>
                      <tbody>
                        <tr>
                          <th scope="row">沿革</th>
                          <td>2003年4月 NPO多文化共生支援センターとして発足<br>現在にいたる。</td>
                        </tr>
                        <tr>
                          <th scope="row">役員名簿</th>
                          <td>
                          理事長　中澤　百百子<br>
                          役　員　小川　茂<br>
                          役　員　辻嶋　彰<br>
                          役　員　加賀見　岩雄<br>
                          役　員　片山　邦夫<br>
                          役　員　嶋野　明正<br>
                          役　員　田中　武治<br>
                          役　員　大村　隆志<br>
                          役　員　小宮　誉文<br>
                          監　事　金本　敏男
                          <p class="txt-right">2017年1月現在</p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
    
                </div>
                <div class=" right-cont">
                    <table>
                      <tbody>
                        <tr>
                          <th scope="row">参加学校法人</th>
                          <td>UNDER CONSTRACTION</td>
                        </tr>
                        <tr>
                          <th scope="row">参加一般企業<br></th>
                          <td>アシアナスタッフサービス株式会社<br>
                          三井ホーム株式会社<br>
                          金本会計事務所　他</td>
                        </tr>
                        <tr>
                          <th scope="row">設立</th>
                          <td>2003年4月</td>
                        </tr>
                        <tr>
                          <th scope="row">理事長</th>
                          <td>中澤百百子（Momoko NAKAZAWA）</td>
                        </tr>
                        <tr>
                          <th scope="row">連絡先</th>
                          <td>〒113-0022<br>
                          東京都文京区千駄木5-49-1<br><a href="https://goo.gl/maps/xaxHEhxogK42" target="_blank">google mapで見る</a><br>
                          TEL : 03-6712-6958<br>
                          FAX : 03-6712-6959<br>
                          E-mail : info@tabunkanpo.org<br>
                          HP : http://www.tabunkanpo.org</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
        	</div>
           
          <p class="pdf-link txt-center"><img src="images/material-title-blue.png" width="108" height="17" alt="MATERIAL"/><a href="pdf/Non Profitable Organization Multicultural Symbiotic Support Center.pdf" target="_blank">Non Profitable Organization Multicultural Symbiotic Support Center</a></p>
        </div><!-- /outline -->
        </section>
        <section>
      <div id="join">
        	<h1><img src="images/join-title.png" width="300" height="93" alt="入会案内　HOW TO JOIN"/></h1>
        <div class="cont-wrap">
       	  <table id="howto">
                  <tbody>
                    <tr>
                      <th scope="row">入会規約・資格</th>
                      <td class="txt-normal">この法人は特定非営利活動法人です。<br>当法人の活動に賛同していただける方は、個人及び団体（企業、学校法人を含む）のどなたでも参加いただけます。運営や運用につきましては、上記の約款をご参照ください。</td>
                    </tr>
                    <tr>
                      <th scope="row">入会金</th>
                      <td>入会金不要</td>
                    </tr>
                    <tr>
                      <th scope="row">会費（年）</th>
                      <td>
                      <table id="fee">
                          <tbody>
                            <tr>
                              <th scope="row">学生会員</th>
                              <td>無料</td>
                            </tr>
                            <tr>
                              <th scope="row">一般会員</th>
                              <td>3,000円</td>
                            </tr>
                            <tr>
                              <th scope="row">法人会員</th>
                              <td>20,000円</td>
                            </tr>
                            <tr>
                              <th scope="row">賛助会員</th>
                              <td>1口以上<br>（1口　4,000円）</td>
                            </tr>
                          </tbody>
                        </table>
                        事業年度は4月1日から翌年の3月31日までとする。<br>
                        但し、初年度の年会費は、入会月から次の3月までの月数×年会費の1/12とする。<br>
                        (百円未満は切り上げ)(賛助会員は除く)
                      </td>
                    </tr>
                  </tbody>
            </table>
            
            <div class="button"><a href="join-jp/" target="_blank"><img src="images/btn-form-jp.png" width="350" height="90" alt="Jpanese form entry 【日本語版】お申込みフォームはこちら"/></a><a href="join-en/" target="_blank"><img src="images/btn-form-en.png" width="350" height="90" alt="English form entry 【英語版】お申込みフォームはこちら"/></a></div>
            
           </div> 
</div><!-- /join -->
</section>

<?php include_once('mailform.php'); ?>

</div>

<?php get_footer(); ?>