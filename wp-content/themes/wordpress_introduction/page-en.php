<?php
/*
Template Name: TOPページ（英語）
*/
?>

<?php get_header(); ?>

<div id="slider" class="wrap">
	<ul class="bxslider">
		<li><img src="<?php echo esc_url( home_url() ); ?>/images/main-img-01.jpg" alt="" /></li>
		<li><img src="<?php echo esc_url( home_url() ); ?>/images/main-img-02.jpg" alt="" /></li>
		<li><img src="<?php echo esc_url( home_url() ); ?>/images/main-img-03.jpg" alt="" /></li>
	</ul>
</div>
<div id="content" class="wrap">
<section>

<br>
<br>

<h1><img src="<?php echo esc_url( home_url() ); ?>/images/news-title.png" width="136" height="94" alt="新着情報　NEWS"/></h1>



<div id="info-en">

<dl>
<dt><strong>January 11, 2018 (Thu.)</strong></dt>
<dd><strong>Start from 9:30 am., February 17, 2018 (Sat.)<br>【The 2nd. Career Support Seminar】</strong>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo esc_url( home_url() ); ?>/images/newlogo.gif" width="58.8" height="25.2" alt="new" /><br>
<strong><font size="3" color="blue"><u>『DON'T be scared in employment interviews!』</u></font></strong> 
<br>Do you interest in how to get employment in Japan? Do you want to get employment in Japan? Tabunka support center will help you find the answer!! <br>
Part 1: Interview tips.<br>
Part 2: Company introduction. <br>
Entry Fee: FREE<br>
Limit in: 50 people<br>
【Inquire and Application】<a href="mailto:info@tabunkanpo.org">✉Mail：info@tabunkanpo.org<a/>
</dd>
</dl>

<dl>
<dt><strong>January 11, 2018 (Thu.)</strong></dt>
<dd><strong>Start from 18:00 am., February 02, 2018 (Fri.)<br>【The 2nd. Tabunka Seminar】</strong> &nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo esc_url( home_url() ); ?>/images/newlogo.gif" width="58.8" height="25.2" alt="new" />
<br>Have you ever heard the word "public diplomacy"?<br>
It is a new diplomacy that makes relationships with overseas citizens and encourages public opinion through media and cultural exchange.<br>
Let’s think about what "culture" is, what "Japan" is, and how to "introducing Japan" . <br>
English is also available. <br>
Entry Fee: FREE<br>
【Inquire and Application】<a href="mailto:info@tabunkanpo.org">✉Mail：info@tabunkanpo.org<a/>
</dd>
</dl>

<dl>
<dt><strong>November 14, 2017 (Tue.)</strong></dt>
<dd><strong>【The 4th. International Christmas Party】</strong> Start from 3:00 pm., December 9, 2017 (Sat.)<br>
While you are visting in Japan, would you like to experience hospitality with the people from all over the world? <br>
Joining us now! Let’s get the party on!<br>
People in charge of finding emploment in Japanese companies will also participate.<br>
Address:DUBLINERS' IRISH PUB AKASAKA 2-11-1 Nagatacho, Chiyoda-ku, Sanno Park Tower B1F<br>
Entry Fee: Students, 2000 Yen per person.<br>
General, 3000 Yen per person<br>
Limit in: 30 people
<a href="<?php echo esc_url( home_url() ); ?>/pdf/xmas2017en.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />English</a>
<br><font color="red">※Finished.</font></dd>
</dl>

<dl>
<dt><strong>November 1, 2017 (Wed.)</strong></dt>
<dd><strong>【ANIMENYC 2017】</strong> November 17~19, 2017 (Fri.,Sat.,Sun.)<br>
ANIMENYC is one of the biggest anime conventions in New York City!!<br>
This time!!We are going to join this big show aims to promote Japanese traditional cultural!<br>
<strong>DON'T MISS OUT!</strong><br>
 Come and Find us in<font color="blue"><strong><u>3E HALL, booth #203</u></strong></font><br>
Address:JAVITS CENTER, 665W 34th St, New York, NY 10001
<a href="<?php echo esc_url( home_url() ); ?>/pdf/AnimeNYC-en.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />English</a><font color="red">※Finished.</font></dd>
</dl>

<dl>
<dt><strong>October 27, 2017 (Fri.)</strong></dt>
<dd><strong>【Tabunka Seminar】</strong> Start from 6:00 pm., November 7, 2017 (Tue.)<br>
Have you ever considered what does "multicultural" mean? <br>
For example, have you ever felt strange about Japanese food culture?<br>
Let's disscuss and find the answer!!!<br>
Address:Non-profit Organization Multicultural Symbiotic Support Center<br>
        5-49-1 Sendagi, Bunkyo-ku, Tokyo, 113-0022 Japan
Entry Fee:FREE<br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/tabunkasemi1.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語</a>
<font color="red">※Finished.</font></dd>
</dl>


<dl>
<dt><strong>January 12, 2017 (Wed.)</strong></dt>
<dd><strong>【Career Support Seminar for international student】</strong> Start from 1:30 pm., February 4, 2017 (Sat.) <br>
[For your successful interview]<br> 
Seminar aims to international student who wants to get employment in Japan ( Share information and interview tips about Japanese company)<br>
Part 1: Company introduction. <br>
Part 2: Interview tips.<br>
Entry Fee: FREE!<br>
Limit in: 50 people<br>
Do you interest in how to get employment in Japan? Do you want to get employment in Japan? Tabunka support center will help you find the answer!! </br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/shusyokuannai2017.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />就職セミナー案内</a>　<a href="<?php echo esc_url( home_url() ); ?>/pdf/shukatsu2017.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />申込書PDF</a>　<a href="<?php echo esc_url( home_url() ); ?>/pdf/shukatsu2017.docx" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/wordicon_large.png" width="20" height="20" alt="pdf" />申込書word</a><font color="red">※Finished.</font></dd>
</dl>
      
<dl>
<dt><strong>November 9, 2016 (Wed.)</strong>　
<dd><strong>【The 3rd. International Christmas Party】</strong> Start from 3:00 pm., December 10, 2016 (Sat.) 
For people from all over the world, let’s get party on! 
Address: Roppongi Bar Del Sole: 1F Palata Roppongi, 6-8-14 Roppongi, Minato-ku, Tokyo. 106-0032
Entry Fee: Students, 2000 Yen per person.
 General, 3000 Yen per person</br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/xmas2016.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語版</a><font color="red">※Finished.</font></dd>
</dl>

<dl>     
<dt><strong>January 25, 2016 (Mon.)</strong></dt>
<dd><strong>【Let’s discover Egypt’s eating habits 】</strong> Start from 2:00 pm. to 4:00 pm., February 2, 2016 (Sat.) 
You may know pyramid or sphinx, but do you know the Egypt’s foods or eating habits? Won’t you want to know? 
Let’s discover the truth.
Address: NPO tabunka support center: B-102 Rapport Minamiaoyama, 5-4-44 Minamiaoyama, Minato-ku, Tokyo. 106-0032
Entry Fee: FREE!</br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/egypt_cooking_japan.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語版</a><font color="red">※Finished.</font></dd>
</dl>


<dl>
<dt><strong>December 17, 2015 (Tue.)</strong></dt>
<dd><strong>【2days and 1 night Ski Tour】</strong> from February. 5th, 2016 (Fri.) to Feb. 6th, 2016 (Sat.) 
Muikamachi ski resort (Niigata-ken), having fun with winter in 2016! 
※We will stop accepting applications once all the places are taken. For more information, please check</br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/Ski tour.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語版</a>・<a href="<?php echo esc_url( home_url() ); ?>/pdf/Ski tour_english.pdf" style="text-decoration:none" target="_blank" alt="pdf" />English版</a><font color="red">※Finished.。</font></dd>
</dl>

<dl>
<dt><strong>November 9, 2015 (Mon.)</strong></dt>
<dd><strong>【Roll Sushi Party】</strong> Start from 2:00 pm. to 4:00 pm., December 12, 2015 (Sat.)
We will invite the Sushi cook from the famous Sushi restaurant with over 50 years of history in Akasaka, to give the tutorial of how to roll a Sushi. 
For more information, please check</br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/Sushi party.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語版</a>・<a href="<?php echo esc_url( home_url() ); ?>/pdf/Sushi party_english.pdf" style="text-decoration:none" target="_blank" alt="pdf" />English版</a><font color="red">※Finished.</font></dd>
</dl>

<dl> 
<dt><strong>October 21, 2015 (Wed.)</strong></dt>
<dd><strong>【Information of Rental space】</strong>
In this time, Non Profit Organization Multicultural symbiotic support center will open a space for rental space in Omotesandou, for worldwide students can get a place to communicate.  
For more information, please check</br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/rental space.pdf" style="text-decoration:none" target="_blank" alt="pdf" />詳細はこちら<img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" /></a><a href="<?php echo esc_url( home_url() ); ?>/pdf/rental space.pdf" style="text-decoration:none" target="_blank" alt="pdf" /></a><font color="red">※Finished.</font></dd>
</dl>

<dl>
<dt><strong>October 21, 2015 (Wed.)</strong></dt>
<dd><strong>【Kawagoe Shichifukujin Meguri】</strong>Start from 10:00 am. , July 18, 2015 (Sat.)
Shichifukujin means 7 deities of good luck in Japanese. This time a tour of Shichifukujin will be conducted in Edo area, Kawagoe-shi.
For more information, please check</br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/kawagoe Seven Lucky Gods Tour.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語版</a>・<a href="<?php echo esc_url( home_url() ); ?>/pdf/kawagoe Seven Lucky Gods Tour_english.pdf" style="text-decoration:none" target="_blank" alt="pdf" />English版</a><font color="red">※Finished.</font></dd>
</dl>

<dl>
<dt><strong>October 21, 2015 (Fri.)</strong></dt>
<dd><strong>【Zoushigaya Shichifukujin Meguri】</strong>Start from 10:00 am. , May 29, 2015 (Sat.)
Shichifukujin means 7 deities of good luck in Japanese. This time a tour of Shichifukujin will be conducted in Tokyo, Zoushigaya, which is nearby Ikebukuro.
For more information, please check</br>
<a href="<?php echo esc_url( home_url() ); ?>/pdf/zoshigaya Seven Lucky Gods Tour.pdf" style="text-decoration:none" target="_blank" alt="pdf" /><img src="<?php echo esc_url( home_url() ); ?>/images/pdficon_large.png" width="20" height="20" alt="pdf" />日本語版</a>・<a href="<?php echo esc_url( home_url() ); ?>/pdf/zoshigaya Seven Lucky Gods Tour_english.pdf" style="text-decoration:none" target="_blank" alt="pdf" />English版</a><font color="red">※Finished.</font></dd>
</dl>

<dl>
<dt><strong>March 12, 2015 (Thu.)</strong></dt>
<dd><strong>【Facebook page】</strong>started!
Find out more in our Facebook homepage</br>
<a href="https://www.facebook.com/pages/%E5%A4%9A%E6%96%87%E5%8C%96%E5%85%B1%E7%94%9F%E6%94%AF%E6%8F%B4%E3%82%BB%E3%83%B3%E3%82%BF%E3%83%BC/349404461913132?ref=hl" target="_blank">NPO法人 多文化共生支援センター　公式Facebook</a></dd>
</dl>

</div>






<br>
<br>

</section>




<section>
    	<div id="active">
        	<h1><img src="<?php echo esc_url( home_url() ); ?>/images/active-title.png" width="302" height="94" alt="活動報告　ACTIVITIES"/></h1>
            <div class="photo">
                <div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-20.jpg" width="445" height="330" alt="December 9, 2017 The 4th Christmas Party"/>
                  <p>December 9, 2017<br>The 4th Christmas Party</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-19.JPG" width="445" height="330" alt="November 17~19, 2017 ANIMENYC"/>
                    <p>November 17~19, 2017<br>
                    ANIMENYC</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-18.jpg" width="445" height="330" alt="November 8, 2017 The South African Embassy in Japan"/>
                    <p>November 8, 2017<br>
                    The South African Embassy in Japan</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-17.jpg" width="445" height="330" alt="November 7, 2017 Tabunka Seminar"/>
                  <p>November 7, 2017<br>Tabunka Seminar</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-16.JPG" width="445" height="330" alt="February 4, 2017 Career Support Seminar for international student"/>
                  <p>February 4, 2017<br>Career Support Seminar for international student</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-14.jpg" width="445" height="330" alt="February 13, 2016 Event for Egypt’s eating habits"/>
                    <p>February 13, 2016<br>
                    Event for Egypt’s eating habits</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-13.jpg" width="445" height="330" alt="January 29～31, 2016 Ski/Snowboard class"/>
                    <p>January 29～31, 2016<br>
                    Ski/Snowboard class</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-12.jpg" width="445" height="330" alt="December 12, 2015 Roll Sushi party"/>
                    <p>December 12, 2015<br>
                    Roll Sushi party</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-11.jpg" width="445" height="330" alt="September 15, 2015 Watching the grand sumo tournament"/>
                    <p>September 15, 2015<br>
                    Watching the grand sumo tournament</p>
                </div><div class="col">
                  <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-21.jpg" width="445" height="330" alt="June 24, 2015 JAL Sky Museum Tour "/>
                    <p>June 24, 2015<br>
                    JAL Sky Museum Tour</p>
                </div><div class="col">
                    <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-08.jpg" width="445" height="330" alt="February 21, 2015 Ski Tour"/>
                  <p>February 21, 2015<br>
                  Ski Tour</p>
                </div><div class="col">
                    <img src="<?php echo esc_url( home_url() ); ?>/images/active-pict-05.jpg" width="445" height="330" alt="August 26, 2014 Motel house tour, Mitsui Home Co., Ltd."/>
                    <p>August 26, 2014<br>Motel house tour, Mitsui Home Co., Ltd.</p>
                </div>
          </div>
        </div><!-- /active -->
        
            <div id="past-event">
                <div class="past-event-wrap clearfix">
                    <h2><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-title.png" width="172" height="22" alt="PAST EVENTS"/></h2>
                    <ul class="past-photo">
                    <li><h3>The grand sumo tournament</h3>
                            <ul class="col">
                                <li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-01.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-01.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-02.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-02.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-03.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-03.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-04.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-04.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-05.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-05.jpg" width="110" height="80" alt=""/></a></li>
                            </ul>
                        </li>
                    <li>
                            <h3>Swiss tea party seminar</h3>
                            <ul class="col">
                                <li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-11.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-11.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-12.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-12.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-13.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-13.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-14.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-14.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-15.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-15.jpg" width="110" height="80" alt=""/></a></li>
                            </ul>
                        </li>
                        <li><h3>Christmas party</h3>
                            <ul class="col">
                                <li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-06.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-06.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-07.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-07.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-08.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-08.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-09.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-09.jpg" width="110" height="80" alt=""/></a></li><li><a href="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-10.jpg" data-lightbox="pastevent"><img src="<?php echo esc_url( home_url() ); ?>/images/past-event-pict-thumb-10.jpg" width="110" height="80" alt=""/></a></li>
                            </ul>
                        </li>
                        
                                
                            </ul>
                        </li>
                    </ul>
                    </div><!-- /past-event-wrap -->
                    <p id="events-archive"><a href="javascript:commonPop('<?php echo esc_url( home_url() ); ?>/events_archive.html',950,700);">For more information of past events<span>&raquo;</span></a></p>
                </div><!-- /past-event -->
             </section>   
        <section>
			<div id="about">
                <div class="cont-wrap">
                    <h1><img src="<?php echo esc_url( home_url() ); ?>/images/about-title.png" width="236" height="93" alt="私たちについて　ABOUT5 US"/></h1>
                    <h2><img src="<?php echo esc_url( home_url() ); ?>/images/about-title-02-en.jpg" width="128" height="26" alt="Message from the president:"/></h2>
<p>During my 30 years of working experience at a Japanese language school, I have succeeded in helping over 20,000 international students graduated, to either further their studies or find employment . 
Also, since I got the experience with been exchanged around 20~30 countries, I deeply understand the important of communication, and how difficult to understand each other with different cultural backgrounds.
Thus, “Multicultural Symbiotic Support Center” would like to organize a number of activities that aims to enhance multicultural understandings between Japanese and foreign students.
</p>
                    <p class="txt-serif txt-middle txt-right txt-bold margin-top10">NAKAZAWA Momoko</p>
                </div>
              <div id="purpose" class="clearfix">
                    <div class="cont-wrap">
                        <div id="purpose-title">
                            <h3><img src="<?php echo esc_url( home_url() ); ?>/images/about-col-title.png" width="270" height="80" alt="NPO法人　多文化共生支援センターの目的"/></h3>
                            <p>Our team, "Multicultural symbiosis support center", will provides support to help international students to enhance multicultural understandings and get happier and better further life.</p>
                        </div>
                        <ol>
                            <li><img src="<?php echo esc_url( home_url() ); ?>/images/about-col-01-en.jpg" width="333" height="244" alt=""/></li>
                            <li><img src="<?php echo esc_url( home_url() ); ?>/images/about-col-02-en.jpg" width="333" height="244" alt=""/></li>
                            <li><img src="<?php echo esc_url( home_url() ); ?>/images/about-col-03-en.jpg" width="333" height="244" alt=""/></li>
                            <li><img src="<?php echo esc_url( home_url() ); ?>/images/about-col-04-en.jpg" width="333" height="244" alt=""/></li>
                            <li><img src="<?php echo esc_url( home_url() ); ?>/images/about-col-05-en.jpg" width="333" height="244" alt=""/></li>
                        </ol>
                    </div><!-- /cont-wrap -->
                </div><!-- /purpose -->
                <div id="vision">
                    <h3><img src="<?php echo esc_url( home_url() ); ?>/images/vision-title.png" width="174" height="53" alt="ビジョン"/></h3>
                    <p>We organize various support events to help foreigners and Japanese,with different culture and custom, to understand each other better. </p>
                </div><!-- /vision -->
                <div id="mission">
                    <h3><img src="<?php echo esc_url( home_url() ); ?>/images/mission-title.png" width="220" height="55" alt="短期ミッション"/></h3>
                    <p>Tokyo Olympics and Paralympics 2020</br>
We will contribute to developing the guide skills of international students in Japan, to help them can be guider or interpreter for the visitors from their own countries while Tokyo Olympics and Paralympics 2020.
</p>
                </div><!-- /mission -->
              <div id="thema-en">
                    <h3><img src="<?php echo esc_url( home_url() ); ?>/images/5therma-title.png" width="229" height="58" alt="5つの活動テーマ"/></h3>
                   </p>
			</div><!-- /mission -->
	  </div><!-- /about -->
      </section>
      <section>
        <div id="outline" class="clearfix">
        	<h1><img src="<?php echo esc_url( home_url() ); ?>/images/outline-title.png" width="208" height="93" alt="団体概要　OUTLINE"/></h1>
            <div class="clearfix">
                <div class="left-cont">
                    <table>
                      <tbody>
                        <tr>
                          <th scope="row">History</th>
                          <td>Since 2003</td>
                        </tr>
                        <tr>
                          <th scope="row">Member list</th>
                          <td>
                          President   NAKAZAWA Momoko<br>
                          Director    OMURA Takashi<br>
                          Director    TANAKA Takeharu<br>
                          Director    KAGAMI Iwao<br>
                          Officer     KATAYAMA Kunio<br>
                          Officer     SHIMANO Akimasa<br>
                          Officer     KOMIYA Takafumi<br>
                          Officer     OGAWA Shigeru<br>
                          Auditor    TSUJISHIMA Akira<br>
                          Auditor    KANEMOTO Toshio

                          </td>
                        </tr>
                      </tbody>
                    </table>
    
                </div>
                <div class=" right-cont">
                    <table>
                      <tbody>
                        <tr>
                          <th scope="row">School Corporation</th>
                          <td>UNDER CONSTRACTION</td>
                        </tr>
                        <tr>
                          <th scope="row">General Corporation<br></th>
                          <td>Asians staff service Co., Ltd.<br>
                          Mitsui Home Co., Ltd.<br>
                          Kanemoto Accounting Office etc.</td>
                        </tr>
                        <tr>
                          <th scope="row">Establish</th>
                          <td>April, 2003</td>
                        </tr>
                        <tr>
                          <th scope="row">President</th>
                          <td>NAKAZAWA Momoko</td>
                        </tr>
                        <tr>
                          <th scope="row">Address</th>
                          <td>5-49-1 Sendagi, Bunkyo-ku, Tokyo, 113-0022 Japan<br><a href="https://goo.gl/maps/xaxHEhxogK42" target="_blank">google mapで見る</a><br>
                          TEL : 03-6712-6958<br>
                          FAX : 03-6712-6959<br>
                          E-mail : info@tabunkanpo.org<br>
                          HP : http://www.tabunkanpo.org</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
        	</div>
            
          <p class="pdf-link txt-center"><img src="<?php echo esc_url( home_url() ); ?>/images/material-title-blue.png" width="108" height="17" alt="MATERIAL"/><a href="<?php echo esc_url( home_url() ); ?>/pdf/Non Profitable Organization Multicultural Symbiotic Support Center.pdf" target="_blank">Non Profitable Organization Multicultural Symbiotic Support Center</a></p>
        </div><!-- /outline -->
        </section>
        <section>
      <div id="join">
        	<h1><img src="<?php echo esc_url( home_url() ); ?>/images/join-title.png" width="300" height="93" alt="入会案内　HOW TO JOIN"/></h1>
        <div class="cont-wrap">
       	  <table id="howto">
                  <tbody>
                    <tr>
                      <th scope="row">Entrance Requirement and Agreement</th>
                      <td class="txt-normal">As a Non Profit Organization, anyone who agrees with our activity themes, including individual and groups (company, school, etc.) is accepted. For more information, please follow the requirement and agreement before.</td>
                    </tr>
                    <tr>
                      <th scope="row">Entrance Fee</th>
                      <td>FREE.</td>
                    </tr>
                    <tr>
                      <th scope="row">Membership Fee<br>（per year）</th>
                      <td>
                      <table id="fee">
                          <tbody>
                            <tr>
                              <th scope="row">Student Member</th>
                              <td>0 YEN</td>
                            </tr>
                            <tr>
                              <th scope="row">General Member</th>
                              <td>3,000 YEN</td>
                            </tr>
                            <tr>
                              <th scope="row">Corporate Organization</th>
                              <td>20,000 YEN</td>
                            </tr>
                            <tr>
                              <th scope="row">Supporting Member</th>
                              <td>More than<br>One Unit<br>（4,000 YEN<br>each Unit）</td>
                            </tr>
                          </tbody>
                        </table>
                        Financial year starts from 1st. April to 31st. March (nest year)
The annual membership fee for first year should be the number of months （from the enrollment month to the next March.）times 1/12 of the annual membership fee. (Round up below 100 YEN) (Excluding supporting member)
                      </td>
                    </tr>
                  </tbody>
            </table>
            
            <div class="button"><a href="<?php echo esc_url( home_url() ); ?>/join-jp/" target="_blank"><img src="<?php echo esc_url( home_url() ); ?>/images/btn-form-jp.png" width="350" height="90" alt="Jpanese form entry 【日本語版】お申込みフォームはこちら"/></a><a href="<?php echo esc_url( home_url() ); ?>/join-en/" target="_blank"><img src="<?php echo esc_url( home_url() ); ?>/images/btn-form-en.png" width="350" height="90" alt="English form entry 【英語版】お申込みフォームはこちら"/></a></div>
            
           </div> 
</div><!-- /join -->
</section>

	<section>
			<div id="contact">
                <h1><img src="<?php echo esc_url( home_url() ); ?>/images/contact-title.png" alt="お問い合せ　CONTACT" width="227" height="93"/></h1>
                <div class="cont-wrap">
                    <div id="contact_container">
                        <div id="koko"></div> 
						<script>
                        
                            //メールフォーム
                            var filePath = "<?php echo esc_url( home_url() ); ?>/ajaxmail2/";
                            var xmlFile = "<?php echo esc_url( home_url() ); ?>/ajaxmail2/8b29b609f0a70b9256c54f0b8f7ad089.xml";
                            var LastMessage = '<p><strong>メールを送信しました</strong></p>\
                                              <p>　</p>\
                                              <p>お問い合せありがとうございました。</p>\
                                              <p>　</p>\
                                              <p>後程、担当よりご連絡させていただきます。</p>\
                                              <p>よろしくお願いいたします。</p>';
                        </script>   
                    </div>       
			</div>
        </div><!-- /contact -->
    </section>

</div>

<?php get_footer(); ?>