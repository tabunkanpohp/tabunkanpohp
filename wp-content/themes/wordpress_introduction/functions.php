<?php

if ( function_exists('register_sidebar') )
  register_sidebar(array(
        'name'=>'サイドバー',
        'id' => '000',
        'before_widget' => '<div class="sidebar">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
));
  register_sidebar(array(
        'name'=>'サイドバー2',
        'id' => '001',
        'before_widget' => '<div class="sidebar">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
));

//カスタムメニュー
register_nav_menus( array(
    'primary' => 'ヘッダーに入ります',
    'secondary' => 'フッターにに入ります'
));

//アイキャッチ
add_theme_support('post-thumbnails');
set_post_thumbnail_size( 800, 500, true );
add_image_size( 'loop',390, 290, true );

//投稿・固定ページの画像パス
function test_shortcode() {
	ob_start();
	bloginfo('stylesheet_directory');
	$td .= ob_get_clean();
	return $td;
}
add_shortcode('img', 'test_shortcode');


//投稿・固定ページの内部リンク
function test_shortcode2() {
	ob_start();
	echo home_url();
	$td .= ob_get_clean();
	return $td;
}
add_shortcode('home', 'test_shortcode2');


?>