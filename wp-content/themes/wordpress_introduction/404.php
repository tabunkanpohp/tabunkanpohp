<?php get_header(); ?>

<h1>該当ページはありません</h1>
<p>お探しのページが見あたりません。削除された可能性があります。</p>

<?php get_sidebar(); ?>
<?php get_footer(); ?>