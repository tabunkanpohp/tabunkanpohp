<div id="pagetop"><a href="#top"><img src="<?php echo esc_url( home_url() ); ?>/images/pagetop.png" width="76" height="77" alt="ページの先頭へ"/></a></div>

<footer>
Copyright &copy; <?php if ( is_page('14') ): ?>Non-profit Organization Multicultural Symbiotic Support Center<?php else: ?>NPO法人多文化共生支援センター<?php endif; ?>
</footer>

<?php wp_footer(); ?>

</body>
</html>