<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php the_title();?>
<?php the_content(); ?>

<?php endwhile; else: ?>
<p><?php echo "お探しの記事、ページは見つかりませんでした。"; ?></p>
<?php endif; ?>

<?php get_sidebar("2"); ?>
<?php get_footer(); ?>